Source: octave-dataframe
Maintainer: Debian Octave Group <team+pkg-octave-team@tracker.debian.org>
Uploaders: Sébastien Villemot <sebastien@debian.org>
Section: math
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-octave
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-dataframe
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-dataframe.git
Homepage: https://gnu-octave.github.io/packages/dataframe/
Testsuite: autopkgtest-pkg-octave
Rules-Requires-Root: no

Package: octave-dataframe
Architecture: all
Depends: ${misc:Depends},
         ${octave:Depends}
Description: manipulate data in Octave similar to R data.frame
 In the R language, a dataframe object is a way to group tabular data.  The
 functions in this package allow the manipulation of data in a similar way
 in Octave.  Dataframe objects in Octave can be created in a variety of
 ways (from other objects or from tabular data in a file) and then can be
 accessed either as matrix or by column name.
 .
 This Octave add-on package is part of the Octave-Forge project.
